define([
    'uiComponent',
    'Magento_Customer/js/customer-data'
], function (Component, customerData) {
    'use strict';

    return Component.extend({
        tracks: {
            apidata: true
        },
        apidata: function () {
            var v = customerData.get('api-data');
            return v().result || '';
        },
        showMyAPIResult: function () {
            return this.apidata();
        }
    });
})