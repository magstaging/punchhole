<?php
/**
 * Created by PhpStorm.
 * User: hervetribouilloy
 * Date: 16/04/2018
 * Time: 17:31
 */

namespace Mbs\HolePunch\CustomerData;


use Magento\Catalog\Test\Block\Adminhtml\Product\Edit\Section\Options\Type\DateTime;
use Magento\Customer\CustomerData\SectionSourceInterface;

class ApiData implements SectionSourceInterface
{
    /**
     * Get data
     *
     * @return array
     */
    public function getSectionData()
    {
        return [
            'result' => $this->getMyAPIData()
        ];
    }

    private function getMyAPIData()
    {
        return 'This is an API function result followed by date '.(new \DateTime())->format('d-M-y h:m:s');
    }
}